import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import Map from './components/Map'
import Places from './components/Places'
import superagent from 'superagent'
class App extends Component {
  constructor() {
        super()
        this.state = { //you never change state you make a copy and play with that
            venues: []
        }
    }
    componentDidMount() {
        console.log('componentDidMount: ')

        superagent.get('https://api.foursquare.com/v2/venues/search?v=20140806&ll=40.7575285,-73.9884469&client_id=MG4HGWHA3BFAII5ZDYGUZCB1QC2U42XQSZLNJK541FAIZJ1P&client_secret=0UTTGY5JXF0EU5CKMWUCV53OU0IRLD5HL5HX3BOH3CHUYGYS').query(null).set('Accept', 'application/json').end((err, response) => {

            if (err) {
                alert('ERROR: ' + err)
                return
            }
            console.log(JSON.stringify(response.body.response.venues))
            const venues = response.body.response.venues
            this.setState({venues: venues})

        })

    }
    render() {
        const location = {
            lat: 40.7575285,
            lng: -73.9884469
        }

        
        return (
            <div>
                <div style={{
                    width: 300,
                    height: 600,
                    background: 'red'
                }}>
                    <Map center={location} markers={this.state.venues}/>
                </div>
                <Places venues= {this.state.venues}/>
            </div>
        )
    }
}

ReactDOM.render(
    <App/>, document.getElementById('app'))
